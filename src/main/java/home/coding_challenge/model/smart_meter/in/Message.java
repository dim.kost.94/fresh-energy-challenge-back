package home.coding_challenge.model.smart_meter.in;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import home.coding_challenge.model.smart_meter.NotificationType;
import lombok.Data;

@Data
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        property = "type",
        visible = true
)
@JsonSubTypes({
        @JsonSubTypes.Type(value = ControlsMessage.class, name = "CONTROL"),
        @JsonSubTypes.Type(value = ReadingsMessage.class, name = "READINGS")
})
public abstract class Message {
    private NotificationType type;
    private MessagePayload payload;
}
