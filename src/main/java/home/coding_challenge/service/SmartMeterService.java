package home.coding_challenge.service;

import home.coding_challenge.entities.SmartMeter;
import home.coding_challenge.model.smart_meter.in.ControlsMessagePayload;
import home.coding_challenge.model.smart_meter.in.ReadingsMessagePayload;
import home.coding_challenge.model.smart_meter.out.ControlsMessageSumResponse;
import home.coding_challenge.repositories.SmartMeterRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

@Service
@RequiredArgsConstructor
public class SmartMeterService {

    private final SmartMeterRepository smartMeterRepository;
    private final SmartMeterReadingServervice smartMeterReadingServervice;

    @Transactional
    public void recordMeterStatus(ReadingsMessagePayload readingsMessagePayload) {
        SmartMeter meter = createMeterIfNotExists(readingsMessagePayload.getMeterId());
        smartMeterReadingServervice.save(meter, readingsMessagePayload);
    }

    @Transactional
    public SmartMeter createMeterIfNotExists(String meterId) {
        return smartMeterRepository.findById(meterId).orElseGet(() -> smartMeterRepository.save(new SmartMeter().setId(meterId)));
    }

    public ControlsMessageSumResponse calculateMeterSum(ControlsMessagePayload controlsMessagePayload) {
        String meterId = controlsMessagePayload.getMeterId();
        BigDecimal totalSum = smartMeterReadingServervice.getSumOfReadingsForMeterBeforeNow(meterId);
        return ControlsMessageSumResponse.instanceOf(controlsMessagePayload.getMeterId(), totalSum);
    }

    public List<String> findMeters() {
        return smartMeterRepository.findAllIds();
    }
}
