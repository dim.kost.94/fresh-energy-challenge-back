package home.coding_challenge.service;

import home.coding_challenge.entities.SmartMeter;
import home.coding_challenge.entities.SmartMeterReading;
import home.coding_challenge.model.smart_meter.in.MessagePayload;
import home.coding_challenge.model.smart_meter.in.ReadingsMessage;
import home.coding_challenge.model.smart_meter.in.ReadingsMessagePayload;
import home.coding_challenge.repositories.SmartMeterReadingRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class SmartMeterReadingServervice {
    private final SmartMeterReadingRepository smartMeterReadingRepository;


    public SmartMeterReading save(SmartMeter meter, ReadingsMessagePayload readingsMessagePayload) {
        SmartMeterReading reading = SmartMeterReading.valueOf(meter, readingsMessagePayload);
        reading.setMeter(meter);
        return smartMeterReadingRepository.save(reading);
    }

    public BigDecimal getSumOfReadingsForMeterBeforeNow(String meterId) {
        return smartMeterReadingRepository.getSumOfReadingsForMeterBeforeNow(meterId, Timestamp.from(Instant.now()));
    }

    @Transactional
    public Map<String, List<ReadingsMessagePayload>> findReadingsPage(int page, int pageSize) {
        Pageable pageable = PageRequest.of(page, pageSize);
        return smartMeterReadingRepository.findAllMetersReadingsPage(pageable)
                .map(ReadingsMessagePayload::new)
                .collect(Collectors.groupingBy(MessagePayload::getMeterId, Collectors.toList()));
    }

}
