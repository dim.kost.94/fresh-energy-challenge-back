package home.coding_challenge.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;

@Entity
@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
public class SmartMeter {

    @Id
    private String id;

    @JsonIgnore
    @OneToMany(mappedBy = "meter")
    private List<SmartMeterReading> readings;


}
