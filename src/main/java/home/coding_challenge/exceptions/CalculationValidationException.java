package home.coding_challenge.exceptions;

public class CalculationValidationException extends RuntimeException {
    public CalculationValidationException() {
    }

    public CalculationValidationException(String message) {
        super(message);
    }

    public CalculationValidationException(String message, Throwable cause) {
        super(message, cause);
    }

    public CalculationValidationException(Throwable cause) {
        super(cause);
    }

    public CalculationValidationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
