package home.coding_challenge.entities;

import home.coding_challenge.model.smart_meter.in.ReadingsMessagePayload;
import home.coding_challenge.model.smart_meter.in.ReadingsMessagePayloadData;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class SmartMeterReading {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JoinColumn(name = "meter_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private SmartMeter meter;

    private Timestamp timestamp;

    private Integer energy;

    private Integer power;

    @Column(scale = 2)
    private Double voltage;

    @Column(scale = 3)
    private Double frequency;

    public static SmartMeterReading valueOf(SmartMeter meter, ReadingsMessagePayload payload) {
        SmartMeterReading reading = new SmartMeterReading();
        reading.setMeter(meter);
        reading.timestamp = new Timestamp(payload.getTimestamp());
        ReadingsMessagePayloadData data = payload.getData();
        reading.energy = data.getEnergy();
        reading.power = data.getPower();
        reading.voltage = data.getVoltage();
        reading.frequency = data.getFrequency();
        return reading;
    }
}
