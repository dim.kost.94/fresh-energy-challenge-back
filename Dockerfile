FROM openjdk:8-jre-alpine

ADD build/libs/coding-challenge-0.1.0.jar .
ADD lib/readings-server.jar .
ADD start.sh .

ENV READINGS_METERS 5
ENV READINGS_PORT 1002

CMD ["sh", "start.sh"]
