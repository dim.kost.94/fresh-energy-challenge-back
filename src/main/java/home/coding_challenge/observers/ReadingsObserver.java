package home.coding_challenge.observers;

import home.coding_challenge.connectors.WebSocketFrontEndAppConnector;
import home.coding_challenge.model.smart_meter.in.ReadingsMessage;
import home.coding_challenge.model.smart_meter.in.ReadingsMessagePayload;
import home.coding_challenge.model.smart_meter.in.ReadingsMessagePayloadData;
import home.coding_challenge.service.SmartMeterService;
import io.reactivex.Observable;
import io.reactivex.subjects.Subject;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
@Log4j2
@RequiredArgsConstructor
public class ReadingsObserver {

    private final SmartMeterService smartMeterService;
    private final WebSocketFrontEndAppConnector appConnector;

    public Observable<BigDecimal> getSmartMeterReadingsObservable(Subject<ReadingsMessage> messageSubject) {
        return messageSubject
                .map(ReadingsMessage::getPayload)
                .doOnNext(this::persitPayload)
                .doOnNext(this::sendReadingsToClients)
                .map(ReadingsMessagePayload::getData)
                .map(ReadingsMessagePayloadData::getEnergy)
                .map(BigDecimal::new)
                .scan(BigDecimal::add);
    }

    private void sendReadingsToClients(ReadingsMessagePayload readingsMessagePayload) {
        appConnector.submitIndicatorPower(readingsMessagePayload);
    }

    private void persitPayload(ReadingsMessagePayload readingsMessagePayload) {
        smartMeterService.recordMeterStatus(readingsMessagePayload);
    }

}
