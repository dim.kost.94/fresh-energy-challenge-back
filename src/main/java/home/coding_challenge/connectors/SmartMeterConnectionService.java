package home.coding_challenge.connectors;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.*;
import java.net.Socket;
import java.util.concurrent.ExecutorService;

@Log4j2
@Service
@RequiredArgsConstructor
public class SmartMeterConnectionService {

    private final ObjectMapper objectMapper;
    private final ExecutorService executorService;
    private final String SUBMIT_COMMAND = "\n";

    @Value("${readings.host}")
    private String host;

    @SneakyThrows(IOException.class)
    public void sendServerMessage(Object value, int port) {
        try (Socket clientSocket = new Socket(host, port);
             PrintStream out = new PrintStream(clientSocket.getOutputStream())) {
            String json = objectMapper.writeValueAsString(value) + SUBMIT_COMMAND;
            log.trace("request-port: {} | body: {}", port, json);
            out.print(json);
            out.flush();
        }
    }

    public Observable<String> subscribeForPort(int subscriptionPort) {
        Subject<String> subject = PublishSubject.create();
        executorService.execute(() -> this.createPortSubscription(subscriptionPort, subject));
        return subject;
    }

    @SneakyThrows(IOException.class)
    private Disposable createPortSubscription(int subscriptionPort, Subject<String> subject) {
        try (Socket socket = tryConnect(subscriptionPort)) {
            final InputStream socketIn = socket.getInputStream();
            final BufferedReader reader = new BufferedReader(new InputStreamReader(socketIn));
            return Observable.fromIterable(reader.lines()::iterator)
                    .doOnNext(log::trace)
                    .subscribe(subject::onNext);
        }

    }

    @SneakyThrows(InterruptedException.class)
    private Socket tryConnect(int subscriptionPort) {
        log.debug("Trying to connect to port {}", subscriptionPort);
        while (true) {
            try {
                return new Socket(host, subscriptionPort);
            } catch (IOException ignored) {
                log.debug("Could not connect sleeping for a second");
                Thread.sleep(1000L);
            }
        }
    }

}

