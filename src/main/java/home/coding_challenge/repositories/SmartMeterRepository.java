package home.coding_challenge.repositories;

import home.coding_challenge.entities.SmartMeter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.stream.Stream;

public interface SmartMeterRepository extends JpaRepository<SmartMeter, String> {

    @Query("select sm.id from SmartMeter as sm")
    List<String> findAllIds();
}
