package home.coding_challenge.model.smart_meter.in;

import lombok.Data;

import java.util.Objects;

@Data
public class ControlsMessagePayload extends MessagePayload {
    private Integer port;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        ControlsMessagePayload that = (ControlsMessagePayload) o;
        return Objects.equals(port, that.port);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), port);
    }
}
