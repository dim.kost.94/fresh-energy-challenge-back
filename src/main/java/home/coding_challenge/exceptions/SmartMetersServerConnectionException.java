package home.coding_challenge.exceptions;

public class SmartMetersServerConnectionException extends RuntimeException {
    public SmartMetersServerConnectionException() {
    }

    public SmartMetersServerConnectionException(String message) {
        super(message);
    }

    public SmartMetersServerConnectionException(String message, Throwable cause) {
        super(message, cause);
    }

    public SmartMetersServerConnectionException(Throwable cause) {
        super(cause);
    }

    public SmartMetersServerConnectionException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
