package home.coding_challenge.model.smart_meter.in;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class ReadingsMessagePayloadData {
    private Integer energy;
    private Integer power;
    private Double voltage;
    private Double frequency;
}
