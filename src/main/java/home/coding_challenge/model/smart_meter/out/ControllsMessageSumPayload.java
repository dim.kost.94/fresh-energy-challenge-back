package home.coding_challenge.model.smart_meter.out;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
public class ControllsMessageSumPayload {
    private String meterId;
    private BigDecimal sum;
}
