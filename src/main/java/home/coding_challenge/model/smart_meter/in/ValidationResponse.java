package home.coding_challenge.model.smart_meter.in;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ValidationResponse {

    private Long expected;

    private Long received;

}
