package home.coding_challenge.connectors;

import home.coding_challenge.model.smart_meter.in.ReadingsMessagePayload;
import home.coding_challenge.model.smart_meter.out.ControllsMessageSumPayload;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class WebSocketFrontEndAppConnector {

    @Value("${app.websocket.subscription.reading.url}")
    private String smartMeterSubscriptionUrl;

    @Value("${app.websocket.subscription.control.url}")
    private String smartMeterControllTopic;

    private final SimpMessagingTemplate simpMessagingTemplate;

    public void submitIndicatorPower(ReadingsMessagePayload messagePayload) {
        String topic = smartMeterSubscriptionUrl.replaceAll("\\{meter_id\\}", messagePayload.getMeterId());
        simpMessagingTemplate.convertAndSend(topic, messagePayload);
    }

    public void submitIndicatorTotalEnergy(ControllsMessageSumPayload sumPayload) {
        String topic = smartMeterControllTopic.replaceAll("\\{meter_id\\}", sumPayload.getMeterId());
        simpMessagingTemplate.convertAndSend(topic, sumPayload);
    }

}
