package home.coding_challenge.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import home.coding_challenge.connectors.SmartMeterConnectionService;
import home.coding_challenge.model.smart_meter.NotificationType;
import home.coding_challenge.model.smart_meter.in.ControlsMessage;
import home.coding_challenge.model.smart_meter.in.Message;
import home.coding_challenge.model.smart_meter.in.ReadingsMessage;
import home.coding_challenge.observers.ControllsObserver;
import home.coding_challenge.observers.ReadingsObserver;
import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PreDestroy;
import java.io.IOException;
import java.math.BigDecimal;

@Log4j2
@Service
@RequiredArgsConstructor
public class SmartMeterSignalsInterprenter {

    private final SmartMeterConnectionService connectionService;
    private final ControllsObserver controllsObserver;
    private final ObjectMapper objectMapper;
    private final ReadingsObserver readingsObserver;
    private final CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Value("${readings.port}")
    private Integer port;

    @Value("${readings.meters}")
    private Integer meters;

    @PreDestroy
    public void destroy() {
        compositeDisposable.dispose();
    }

    public void subscribeForSmartMetersReadings() {
        this.connectionService.subscribeForPort(port)
                .map(this::readMessage)
                .groupBy(m -> m.getPayload().getMeterId())
                .map(group -> {
                    final Subject<ReadingsMessage> messageSubject = PublishSubject.create();
                    final Subject<ControlsMessage> controlSubject = PublishSubject.create();
                    Observable<BigDecimal> acumulatedReadingsObservable = readingsObserver.getSmartMeterReadingsObservable(messageSubject);
                    this.controllsObserver.subscribeForControllEvents(group.getKey(), acumulatedReadingsObservable, controlSubject);
                    return group.subscribe(groupMessage -> {
                        if (NotificationType.READINGS.equals(groupMessage.getType())) {
                            messageSubject.onNext((ReadingsMessage) groupMessage);
                        }
                        if (NotificationType.CONTROL.equals(groupMessage.getType())) {
                            controlSubject.onNext((ControlsMessage) groupMessage);
                        }
                    });
                })
                .subscribe();
    }


    @SneakyThrows(IOException.class)
    private Message readMessage(String jsonLine) {
        return objectMapper.readValue(jsonLine, Message.class);
    }

}
