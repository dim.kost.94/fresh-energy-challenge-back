package home.coding_challenge.model.smart_meter.in;

import lombok.Data;

import java.util.Objects;

@Data
public class ReadingsMessage extends Message {
    private ReadingsMessagePayload payload;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        ReadingsMessage that = (ReadingsMessage) o;
        return Objects.equals(payload, that.payload);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), payload);
    }
}
