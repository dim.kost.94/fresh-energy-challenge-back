package home.coding_challenge.exceptions;

public class MeterServerExecutionException extends RuntimeException {
    public MeterServerExecutionException() {
    }

    public MeterServerExecutionException(String message) {
        super(message);
    }

    public MeterServerExecutionException(String message, Throwable cause) {
        super(message, cause);
    }

    public MeterServerExecutionException(Throwable cause) {
        super(cause);
    }

    public MeterServerExecutionException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
