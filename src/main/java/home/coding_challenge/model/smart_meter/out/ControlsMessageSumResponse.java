package home.coding_challenge.model.smart_meter.out;

import home.coding_challenge.model.smart_meter.NotificationType;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

@Data
@Accessors(chain = true)
public class ControlsMessageSumResponse {

    private NotificationType type = NotificationType.SUM_CONTROL;
    private ControllsMessageSumPayload payload;

    public static ControlsMessageSumResponse instanceOf(String meterId, BigDecimal sum) {
        ControlsMessageSumResponse response = new ControlsMessageSumResponse();
        response.payload = new ControllsMessageSumPayload()
                .setMeterId(meterId)
                .setSum(sum);
        return response;
    }
}
