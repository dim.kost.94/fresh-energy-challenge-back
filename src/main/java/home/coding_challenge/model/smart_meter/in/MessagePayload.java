package home.coding_challenge.model.smart_meter.in;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public abstract class MessagePayload {
    private String meterId;
}
