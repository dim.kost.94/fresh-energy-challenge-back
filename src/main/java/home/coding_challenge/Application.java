package home.coding_challenge;

import home.coding_challenge.service.SmartMeterSignalsInterprenter;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;

import javax.annotation.PostConstruct;

@PropertySource({"classpath:custom.properties"})
@SpringBootApplication
@RequiredArgsConstructor
public class Application {

    private final SmartMeterSignalsInterprenter signalsInterprenter;
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @PostConstruct
    public void init() {
        //entry point
        signalsInterprenter.subscribeForSmartMetersReadings();
    }

}
