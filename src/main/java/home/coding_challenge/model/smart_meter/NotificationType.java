package home.coding_challenge.model.smart_meter;


public enum NotificationType {
    READINGS, CONTROL, SUM_CONTROL
}
