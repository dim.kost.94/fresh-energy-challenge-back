package home.coding_challenge.repositories;

import home.coding_challenge.entities.SmartMeterReading;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.stream.Stream;

public interface SmartMeterReadingRepository extends JpaRepository<SmartMeterReading, Long> {
    @Query("select sum(sm_read.energy) " +
            "from SmartMeterReading as sm_read " +
            "where sm_read.meter.id=:meterId " +
            "and sm_read.timestamp < :until ")
    BigDecimal getSumOfReadingsForMeterBeforeNow(@Param("meterId") String meterId, @Param("until") Timestamp until);


    @Query("select sm_reading from SmartMeterReading as sm_reading " +
            "inner join fetch sm_reading.meter " +
            "order by sm_reading.timestamp desc ")
    Stream<SmartMeterReading> findAllMetersReadingsPage(Pageable pageable);
}
