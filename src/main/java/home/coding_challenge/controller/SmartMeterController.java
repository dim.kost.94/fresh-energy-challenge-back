package home.coding_challenge.controller;

import home.coding_challenge.model.smart_meter.in.ReadingsMessagePayload;
import home.coding_challenge.service.SmartMeterReadingServervice;
import home.coding_challenge.service.SmartMeterService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequiredArgsConstructor
public class SmartMeterController {

    private final SmartMeterService smartMeterService;
    private final SmartMeterReadingServervice smartMeterReadingsService;

    @GetMapping("/api/meters/readings")
    public Map<String, List<ReadingsMessagePayload>> getSmartMetersReadings(@RequestParam(value = "page", required = false, defaultValue = "0") int page,
                                                                            @RequestParam(value = "pageSize", required = false, defaultValue = "100") int pageSize) {
        return smartMeterReadingsService.findReadingsPage(page, pageSize);
    }

    @GetMapping("/api/meters")
    public List<String> getSmartMeters() {
        return smartMeterService.findMeters();
    }

}
