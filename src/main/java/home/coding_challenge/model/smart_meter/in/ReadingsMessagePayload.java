package home.coding_challenge.model.smart_meter.in;

import home.coding_challenge.entities.SmartMeterReading;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.Objects;

@Data
@NoArgsConstructor
@Accessors(chain = true)
public class ReadingsMessagePayload extends MessagePayload {
    private Long timestamp;
    private ReadingsMessagePayloadData data;

    public ReadingsMessagePayload(SmartMeterReading message) {
        this.setData(
                new ReadingsMessagePayloadData()
                        .setEnergy(message.getEnergy())
                        .setFrequency(message.getFrequency())
                        .setPower(message.getPower())
                        .setVoltage(message.getVoltage())
        );
        this.setTimestamp(message.getTimestamp().getTime());
        this.setMeterId(message.getMeter().getId());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        ReadingsMessagePayload that = (ReadingsMessagePayload) o;
        return Objects.equals(timestamp, that.timestamp) &&
                Objects.equals(data, that.data);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), timestamp, data);
    }
}
