package home.coding_challenge.observers;

import com.fasterxml.jackson.databind.ObjectMapper;
import home.coding_challenge.connectors.SmartMeterConnectionService;
import home.coding_challenge.connectors.WebSocketFrontEndAppConnector;
import home.coding_challenge.exceptions.CalculationValidationException;
import home.coding_challenge.model.smart_meter.in.ControlsMessage;
import home.coding_challenge.model.smart_meter.in.ControlsMessagePayload;
import home.coding_challenge.model.smart_meter.in.ValidationResponse;
import home.coding_challenge.model.smart_meter.out.ControllsMessageSumPayload;
import home.coding_challenge.model.smart_meter.out.ControlsMessageSumResponse;
import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

@Log4j2
@Service
@RequiredArgsConstructor
public class ControllsObserver {

    private final ObjectMapper objectMapper;
    private final SmartMeterConnectionService connectionService;
    private final CompositeDisposable compositeDisposable = new CompositeDisposable();
    private final WebSocketFrontEndAppConnector frontEndAppConnector;

    public void subscribeForControllEvents(String meterId, Observable<BigDecimal> accumulatedReadingsObservable,
                                           Observable<ControlsMessage> controlsMessageObservable) {
        AtomicReference<BigDecimal> acumulatedEnergy = new AtomicReference<>();
        Disposable meterDisposable = accumulatedReadingsObservable
                .doOnNext(m -> frontEndAppConnector.submitIndicatorTotalEnergy(new ControllsMessageSumPayload(meterId, m)))
                .subscribe(acumulatedEnergy::set);
        Disposable controllsDisposable = controlsMessageObservable
                .map(ControlsMessage::getPayload)
                .subscribe(controlsMessagePayload -> this.sendResponse(controlsMessagePayload, acumulatedEnergy.get()));
        compositeDisposable.addAll(controllsDisposable, meterDisposable);
    }

    private ControllsMessageSumPayload sendResponse(ControlsMessagePayload controlsMessagePayload, BigDecimal bigDecimal) {
        Integer receivedPort = controlsMessagePayload.getPort();
        ControlsMessageSumResponse response = ControlsMessageSumResponse.instanceOf(controlsMessagePayload.getMeterId(), bigDecimal);
        connectionService.sendServerMessage(response, receivedPort);
        this.subscribeForValidationResponse(receivedPort);
        return response.getPayload();
    }

    private void subscribeForValidationResponse(Integer port) {
        AtomicBoolean handled = new AtomicBoolean();
        //todo add implementation to dispose right after the message has bean received
        this.connectionService.subscribeForPort(port)
                .map(this::payload)
                .subscribe(validationResponse -> {
                    long delta = validationResponse.getExpected() - (long) validationResponse.getReceived();
                    if (Math.abs(delta) > 50L) {
                        throw new CalculationValidationException();
                    } else if (delta > 10L) {
                        log.warn("Delta is still higher then one!");
                    }
                    handled.set(true);
                });
    }


    @SneakyThrows(IOException.class)
    private ValidationResponse payload(String json) {
        return objectMapper.readValue(json, ValidationResponse.class);
    }

}
